from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from polls import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pythonmail.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'settings', views.settings, name='settings'),
    url(r'login', views.login, name='login'),
    url(r'logout', views.logout, name='logout'),
    url(r'question/(\d+)', views.question, name='question'),
    url(r'registration', views.registration, name='registration'),
    url(r'vote', views.vote, name='vote'),
    url(r'^$', views.index),
 #   url(r'^admin/', include(admin.site.urls)),
)
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
