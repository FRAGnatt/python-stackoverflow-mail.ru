#coding = utf-8
import sys
from django.core.management.base import BaseCommand, CommandError
from mixer import fakers as f
import random
import hashlib

def myinya(id):
    return [id,"",0,id]

def generateFakeUserInformation():
    nickname = f.get_username()
    login = nickname
    email = f.get_email(nickname)
    password = hashlib.sha512("12345").hexdigest()
    rating = 0
    registration = "2014-26-06 15:00:00"
    return [password, "", False, login,"","", email, 0, 1,registration]

def generateFakeQuestionInformation():
    title = f.get_lorem(25)
    content = f.get_lorem(500)
    author_id = random.randint(1,100)
    created_date = "2014-11-07 12:00:00"
    return (title, content, created_date,0, author_id)

def generateQuestionTag(i):
    tag = random.randint(1,6)
    return (i,tag)

def generateFakeAnswerInformation():
    content = f.get_lorem(250)
    author = random.randint(1, 100)
    question = random.randint(1, 100)
    right = 0
    date = "2014-11-08 11:00:00"
    return (date,content,right,author, question)

def writeInCSV(args,name=None):
    f = open(name+".csv", "a")
    for i,item in enumerate(args):
        if (i == 0):
            if (isinstance(item,int)):
                f.write(str(item))
            else:
                f.write('"'+item+'"')
        else:
            if (isinstance(item,int)):
                f.write(','+str(item))
            else:
                f.write(',"'+item+'"')
    f.write('\r\n')
    f.close()

class Command(BaseCommand):
    help = 'bla'

    def handle(self, *args, **options):
        for i in xrange(1, 101):
            writeInCSV(generateFakeUserInformation(),name="django_user")
            print "django_user Creates: "+str(i)
        for i in xrange(1, 102):
            writeInCSV(myinya(i),name="myuser")
            print "myuser Creates: "+str(i)
        for i in xrange(1, 100):
            writeInCSV(generateFakeQuestionInformation(),name="question")
            print "question Creates: "+str(i)
        for i in xrange(1, 100):
            writeInCSV(generateQuestionTag(i),name="question_tag")
            writeInCSV(generateQuestionTag(i),name="question_tag")
            print "question_tag Creates: "+str(i)