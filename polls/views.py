from django.shortcuts import render, redirect
from polls import models
from django.http import HttpRequest
from django.contrib import auth
from django.core.paginator import Paginator
from django.db.models import Count
from django.contrib.auth.models import User
from django.http import HttpResponse


def index(req):
    if (req.POST.get("title")  and req.POST.get("text")):
        if not req.user.is_authenticated():
            return redirect("/")
        else:
            question = models.Question(
                title = req.POST['title'],
                text = req.POST['text'],
                author_id = req.user.id,
                raiting = 5,
            )
            question.save()
            if (req.POST.get('tag_list')):
                tag_list = req.POST['tag_list']
                import re
                tag_list = re.split(',|\s',tag_list)
                for tag in tag_list:
                    from django.core.exceptions import ObjectDoesNotExist
                    try:
                        dbTag = models.Tag.objects.get(tag = tag)
                    except ObjectDoesNotExist:
                        dbTag = models.Tag(tag = tag)
                        dbTag.save()
                    question.tags.add(dbTag)
                question.save()
    page = req.GET.get('page',1)
    filter = req.GET.get('sort','raiting')
    if (filter == 'raiting'):
        paginator = Paginator(models.Question.objects.prefetch_related('tags').order_by('-date'), 5)
    else:
        paginator = Paginator(models.Question.objects.prefetch_related('tags').order_by('-raiting'), 5)
    paginator_page = paginator.page(page)
    return render(req, 'main.html',{"question_list":paginator_page,"req":req})

def vote(req):
    import json
    if not req.user.is_authenticated():
        ans = {"status" : 0, "message" : "Not allowed"}
        ans = json.dumps(ans)
        return HttpResponse(ans, content_type="application/json")
    else:
        vote = req.POST.get('type')
        if(vote == "down"):
            vote = -1
        if(vote == "up"):
            vote = 1
        questionId = int(req.POST.get('question_id'))
        userId = int(req.user.id)
        from django.core.exceptions import ObjectDoesNotExist
        try:
            hasVoted = models.Raiting.objects.get(question_id=questionId, user_id=userId)
        except ObjectDoesNotExist:
            q = models.Question.objects.get(id=questionId)
            q.raitingg = q.raitingg + vote
            raitingDB = models.Raiting(question_id = questionId, user_id = userId, vote = int(vote) )
            raitingDB.save()
            q.save()
            ans = {"status" : 1, "new_rating" : str(q.raitingg)}
            ans = json.dumps(ans)
            return HttpResponse(ans, content_type="application/json")
        ans = {"status" : 0, "message" : "User has already voted"}
        ans = json.dumps(ans)
        return HttpResponse(ans, content_type="application/json")


def question(req,question_id):
    questionDef = models.Question.objects.prefetch_related('tags').get(pk=question_id)
    answer_list = models.Answer.objects.all().filter(question=question_id).prefetch_related('author_id')
    if (req.POST.get("text")):
        if not req.user.is_authenticated():
            return redirect("/")
        else:
            answer = models.Answer(
                text = req.POST['text'],
                author_id_id = req.user.id,
                question_id =  question_id
            )
            answer.save()
    return render(req,'question.html',{"question" : questionDef, "answer_list":answer_list,"req":req})


def login(req):
    from django.core.context_processors import csrf
    if (req.POST.get("login")):
        user = auth.authenticate(username=req.POST.get("login"), password=req.POST.get("password"))
        if user is not None:
            auth.login(req, user)
            return redirect("/")
        else:
            return render(req, 'login.html',{"error":True})
    else:
        return render(req, 'login.html',{"error":False})

def logout(req):
    auth.logout(req)
    return redirect("/")


def settings(req):
    return render(req, 'settings.html')
def registration(req):
    if(req.POST):
        if (req.POST['password'] != req.POST['remember_password']):
            return redirect("/registration")
        login = req.POST['login']
        password = req.POST['password']
        email = req.POST['email']
        newUser = User.objects.create_user(login, email, password)
        profile = models.MyUser(raiting = 5, user_id = newUser.id)
        profile.img = req.FILES['img']
        profile.save()
        user = auth.authenticate(username=login, password=password)
        auth.login(req, user)
        return redirect("/")
    else:
        return render(req,'registration.html')
