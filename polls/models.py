from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class MyUser(models.Model):
    user        = models.OneToOneField(User)
    img      = models.ImageField(upload_to="img/", default="avatar-none.jpg")
    raiting		= models.IntegerField(max_length=11, default = 0)


# class User(models.Model):
#     login 		= models.CharField(max_length=255)
#     email 		= models.CharField(max_length=250)
#     avatar_id 	= models.IntegerField(max_length=11)
#     password 	= models.CharField(max_length=250)
#     nickname 	= models.CharField(max_length=250)
#     registration= models.DateTimeField('date registration')
#     raiting		= models.IntegerField(max_length=11)


class Tag(models.Model):
    tag 		= models.CharField(max_length=255)


class Question(models.Model):
    title 		= models.CharField(max_length=255)
    text		= models.TextField()
    author   	= models.ForeignKey(User)
    date 		= models.DateTimeField(auto_now=True)
    tags        = models.ManyToManyField(Tag)
    raitingg    = models.IntegerField (max_length = 11,default = 0)

class Answer(models.Model):
    author_id 	= models.ForeignKey(User)
    question	= models.ForeignKey(Question)
    date 		= models.DateTimeField(auto_now=True)
    text		= models.TextField()
    flag		= models.BooleanField(default = False)

class Raiting(models.Model):
    user = models.ForeignKey(User)
    question = models.ForeignKey(Question)
    vote = models.IntegerField(default = 1)
    class Meta:
        unique_together = (('user','question'),)